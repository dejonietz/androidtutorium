package de.hdm.tutorium.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdm.tutorium.tutorium.R;

public class CameraActivity extends Activity {

    // <uses-permission android:name="android.permission.CAMERA" />
    // <uses-feature android:name="android.hardware.camera" />

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private boolean mCameraFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        mCameraFound = checkCameraHardware(this);
    }

    // opens camera
    public void openCamera(View view) {
        if (mCameraFound) {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

        } else {
            Toast.makeText(
                    this,
                    "Keine Kamera gefunden!",
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    // checks if this device has a camera
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            Toast.makeText(
                    this,
                    "Kamera gefunden!",
                    Toast.LENGTH_SHORT
            ).show();
            return true;

        } else {
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // image captured
                Toast.makeText(
                        this,
                        "Foto geschossen!",
                        Toast.LENGTH_SHORT
                ).show();

            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled
                Toast.makeText(
                        this,
                        "Abgebrochen!",
                        Toast.LENGTH_SHORT
                ).show();

            } else {
                // image capture failed
            }
        }
    }

}
