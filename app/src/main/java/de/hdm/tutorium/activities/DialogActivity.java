package de.hdm.tutorium.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.Calendar;

import de.hdm.tutorium.tutorium.R;

public class DialogActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
    }

    // creates and shows dialog
    public void openDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppTheme);
        builder.setTitle("Neuer Dialog")
                .setPositiveButton(
                        "Ja",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(DialogActivity.this, "Dialog bestätigt", Toast.LENGTH_SHORT)
                                     .show();
                            }
                        })
                .setNegativeButton("Nein", null)
                .setMessage("Das ist die Meldung im Dialogfenster");
        builder.show();
    }
}
