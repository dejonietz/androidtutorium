package de.hdm.tutorium.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import de.hdm.tutorium.tutorium.R;

public class AsyncTaskActivity extends Activity {

    private static final String URL = "http://ip.jsontest.com";

    private TextView mResultTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asynctask);

        mResultTxt = (TextView) findViewById(R.id.txt_result);
    }

    // starts server communication
    public void startRequest(View view) {
        new ServerAsyncTask()
                .execute();

    }

    private class ServerAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {}

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpGet httpGet = new HttpGet(URL);

                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpResponse response = httpClient.execute(httpGet);
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent(), "UTF-8"), 8192);

                String sResponse = reader.readLine();
                return sResponse;

            } catch (Exception e) {
                Log.e(e.getClass().getName(), e.getMessage(), e);
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (!result.equals("error")) {

                try {
                    JSONObject obj = new JSONObject(result);
                    String onlyIp = obj.getString("ip");
                    mResultTxt.setText("Antwort: " + result
                            + "\n"
                            + "JSON: " + onlyIp);

                } catch (JSONException e) {
                    mResultTxt.setText("Antwort: " + result);
                }


            } else {
                Toast.makeText(
                        AsyncTaskActivity.this,
                        "Fehler bei Serverkommunikation",
                        Toast.LENGTH_SHORT)
                     .show();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
