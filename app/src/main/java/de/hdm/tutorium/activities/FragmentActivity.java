package de.hdm.tutorium.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import de.hdm.tutorium.fragments.SlidingFragment;
import de.hdm.tutorium.tutorium.R;

public class FragmentActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);
    }

    // creates and shows fragment
    public void showFragment(View view) {
        Fragment slidingFragment = new SlidingFragment();

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.addToBackStack("StackName");

        fragmentTransaction.setCustomAnimations(
                R.anim.slide_from_left,
                R.anim.slide_to_left,
                R.anim.slide_from_left,
                R.anim.slide_to_left);

        fragmentTransaction.add(R.id.activity_container, slidingFragment);
        fragmentTransaction.commit();
    }
}
