package de.hdm.tutorium.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import de.hdm.tutorium.tutorium.R;

public class MainActivity extends Activity {

    // activity lifecycle: http://developer.android.com/images/activity_lifecycle.png

    private Button mFormBtn, mDialogBtn, mAnimBtn, mWebviewBtn,
            mSensorBtn, mBluetoothBtn, mFragmentBtn, mGpsBtn, mPrefBtn,
            mNotiBtn, mAsyncTaskBtn, mCameraBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpMainMenu();
    }

    private void setUpMainMenu() {
        // initialize buttons
        mFormBtn = (Button) findViewById(R.id.form_btn);
        mDialogBtn = (Button) findViewById(R.id.dialog_btn);
        mAnimBtn = (Button) findViewById(R.id.anim_btn);
        mWebviewBtn = (Button) findViewById(R.id.webview_btn);
        mSensorBtn = (Button) findViewById(R.id.sensor_btn);
        mBluetoothBtn = (Button) findViewById(R.id.bluetooth_btn);
        mPrefBtn = (Button) findViewById(R.id.pref_btn);
        mFragmentBtn = (Button) findViewById(R.id.fragment_btn);
        mGpsBtn = (Button) findViewById(R.id.gps_btn);
        mNotiBtn = (Button) findViewById(R.id.noti_btn);
        mAsyncTaskBtn = (Button) findViewById(R.id.async_btn);
        mCameraBtn = (Button) findViewById(R.id.camera_btn);

        // set up touchlistener for main menu buttons
        View.OnTouchListener touchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getActionMasked();

                if (action == MotionEvent.ACTION_DOWN) {
                    view.setBackgroundColor(getResources().getColor(R.color.black));
                    ((Button) view).setTextColor(getResources().getColor(R.color.yellow));

                } else if (action == MotionEvent.ACTION_CANCEL) {
                    view.setBackgroundColor(getResources().getColor(R.color.yellow));
                    ((Button) view).setTextColor(getResources().getColor(R.color.black));

                } else if (action == MotionEvent.ACTION_UP) {
                    view.setBackgroundColor(getResources().getColor(R.color.yellow));
                    ((Button) view).setTextColor(getResources().getColor(R.color.black));

                    switch (view.getId()) {
                        case R.id.form_btn:
                            // start FormActivity
                            startActivity(new Intent(MainActivity.this, FormActivity.class));
                            break;

                        case R.id.dialog_btn:
                            // start DialogActivity
                            startActivity(new Intent(MainActivity.this, DialogActivity.class));
                            break;

                        case R.id.anim_btn:
                            // start AnimationActivity
                            startActivity(new Intent(MainActivity.this, AnimationActivity.class));
                            break;

                        case R.id.webview_btn:
                            // start WebviewActivity
                            startActivity(new Intent(MainActivity.this, WebviewActivity.class));
                            break;

                        case R.id.fragment_btn:
                            // start FragmentActivity
                            startActivity(new Intent(MainActivity.this, FragmentActivity.class));
                            break;

                        case R.id.pref_btn:
                            // start PrefActivity
                            startActivity(new Intent(MainActivity.this, PrefsActivity.class));
                            break;

                        case R.id.sensor_btn:
                            // start SensorsActivity
                            startActivity(new Intent(MainActivity.this, SensorsActivity.class));
                            break;

                        case R.id.camera_btn:
                            // start CameraActivity
                            startActivity(new Intent(MainActivity.this, CameraActivity.class));
                            break;

                        case R.id.gps_btn:
                            // start GpsActivity
                            startActivity(new Intent(MainActivity.this, GpsActivity.class));
                            break;

                        case R.id.bluetooth_btn:
                            // start BluetoothActivity
                            startActivity(new Intent(MainActivity.this, BluetoothActivity.class));
                            break;

                        case R.id.noti_btn:
                            // start NotificationActivity
                            startActivity(new Intent(MainActivity.this, NotificationActivity.class));
                            break;

                        case R.id.async_btn:
                            // start AsyncTaskActivity
                            startActivity(new Intent(MainActivity.this, AsyncTaskActivity.class));
                            break;
                    }
                }
                return true;
            }
        };
        mFormBtn.setOnTouchListener(touchListener);
        mDialogBtn.setOnTouchListener(touchListener);
        mWebviewBtn.setOnTouchListener(touchListener);
        mAnimBtn.setOnTouchListener(touchListener);
        mFragmentBtn.setOnTouchListener(touchListener);
        mBluetoothBtn.setOnTouchListener(touchListener);
        mPrefBtn.setOnTouchListener(touchListener);
        mNotiBtn.setOnTouchListener(touchListener);
        mSensorBtn.setOnTouchListener(touchListener);
        mGpsBtn.setOnTouchListener(touchListener);
        mAsyncTaskBtn.setOnTouchListener(touchListener);
        mCameraBtn.setOnTouchListener(touchListener);
    }
}