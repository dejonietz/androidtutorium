package de.hdm.tutorium.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import de.hdm.tutorium.tutorium.R;


public class FormActivity extends Activity {

    private Button mSubmitBtn;
    private EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        mEditText = (EditText) findViewById(R.id.edittext);

        View.OnTouchListener touchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getActionMasked();

                if (action == MotionEvent.ACTION_DOWN) {
                    mSubmitBtn.setTextColor(getResources().getColor(R.color.red));
                } else if (action == MotionEvent.ACTION_UP) {
                    mSubmitBtn.setTextColor(getResources().getColor(R.color.black));

                    submitForm();

                    Intent intent = new Intent(FormActivity.this, SubmitActivity.class);
                    intent.putExtra("Name", mEditText.getText().toString());
                    startActivity(intent);
                }
                return true;
            }
        };
        mSubmitBtn = (Button) findViewById(R.id.submit_btn);
        mSubmitBtn.setOnTouchListener(touchListener);
    }

    public void submitForm() {
        Toast.makeText(this,
                  getResources().getString(R.string.submit),
                Toast.LENGTH_LONG)
             .show();
    }

}


