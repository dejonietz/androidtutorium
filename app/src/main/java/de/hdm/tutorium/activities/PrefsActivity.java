package de.hdm.tutorium.activities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import de.hdm.tutorium.tutorium.R;

public class PrefsActivity extends Activity {

    private static final String PREF_USERNAME = "userName";

    private SharedPreferences prefs;
    private EditText mNameEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prefs);

        mNameEdit = (EditText) findViewById(R.id.nameEdit);

        prefs = getSharedPreferences("PrefsAppName", Context.MODE_PRIVATE);
        String name = prefs.getString(PREF_USERNAME, "Fehler!");
        mNameEdit.setText(name);
    }

    public void saveName(View view) {
        SharedPreferences.Editor e = prefs.edit();
        e.putString(PREF_USERNAME, mNameEdit.getText().toString());
        e.commit();
    }

}
