package de.hdm.tutorium.activities;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

import de.hdm.tutorium.tutorium.R;

public class GpsActivity extends Activity implements LocationListener {

    // <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />

    private TextView mTxtLat, mTxtLon, mTxtAlt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);

        mTxtLat = (TextView) findViewById(R.id.txt_lat);
        mTxtLon = (TextView) findViewById(R.id.txt_lon);
        mTxtAlt = (TextView) findViewById(R.id.txt_alt);

        setUpLocationManager();
    }

    private void setUpLocationManager() {
        LocationManager mLocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // min time and min distance
        mLocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        mTxtLat.setText(String.valueOf(location.getLatitude()));
        mTxtLon.setText(String.valueOf(location.getLongitude()));
        mTxtAlt.setText(String.valueOf(location.getAltitude()));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        // Provider status changed
    }

    @Override
    public void onProviderEnabled(String s) {
        // Provider activated
    }

    @Override
    public void onProviderDisabled(String s) {
        // Provider deactivated
    }



    /* Google Maps:
     * https://developers.google.com/maps/documentation/android/start?hl=de
     */
}
