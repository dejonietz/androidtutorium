package de.hdm.tutorium.activities;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import de.hdm.tutorium.tutorium.R;

public class AnimationActivity extends Activity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        textView = (TextView) findViewById(R.id.textview);
    }

    public void fadeIn(View view) {
        Animation animFadeIn = AnimationUtils.loadAnimation(
                this,
                R.anim.fade_in);

        animFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                // start of animation
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // end of animation
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // called everytime animation is repeated
            }
        });
        textView.startAnimation(animFadeIn);
    }

    public void fadeOut(View view) {
        Animation animFadeOut = AnimationUtils.loadAnimation(
                this,
                R.anim.fade_out);
        textView.startAnimation(animFadeOut);
    }


}
