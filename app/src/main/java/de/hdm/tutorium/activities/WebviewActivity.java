package de.hdm.tutorium.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.Toast;

import de.hdm.tutorium.tutorium.R;

public class WebviewActivity extends Activity {

    private WebView mWebView;
    private EditText mEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        mWebView = (WebView) findViewById(R.id.webview);
        mEditText = (EditText) findViewById(R.id.edittext);
    }

    public void start(View view) {
        String website = mEditText.getText().toString();
        mWebView.loadUrl(website);
    }

}
