package de.hdm.tutorium.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import de.hdm.tutorium.tutorium.R;


public class SubmitActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);

        String value = getIntent().getExtras().getString("Name");
        TextView text = (TextView) findViewById(R.id.text);
        text.setText("Hallo " + value);

    }


}
