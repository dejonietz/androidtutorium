package de.hdm.tutorium.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.hdm.tutorium.tutorium.R;

public class SlidingFragment extends Fragment {

    // fragment lifecycle: http://developer.android.com/images/fragment_lifecycle.png

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sliding, container, false);

        ((TextView) view.findViewById(R.id.textview)).setText("Code");
        return view;
    }
}
